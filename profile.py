#!/usr/bin/python

"""
Profile for attenuator matrix.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
#import geni.rspec.emulab.spectrum as spectrum
#import geni.rspec.emulab.route as route
import geni.rspec.igext as ig

disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-GR38-PACK"


NODE_TYPE = 'nuc5300'

# List of am devices.
am_devices = [
    ("nuc1",
     "nuc1"),
    ("nuc10",
     "nuc10"),
    ("nuc16",
     "nuc16"),
    ("nuc2",
     "nuc2"),
    ("nuc22",
     "nuc22"),
    ("nuc23",
     "nuc23"),
    ("nuc27",
     "nuc27"),
    ("nuc3",
     "nuc3"),
    ("nuc4",
     "nuc4"),
    ("nuc5",
     "nuc5"),
    ("nuc6",
     "nuc6"),
    ("nuc8",
     "nuc8"),
    ("nuc9",
     "nuc9"),
]


# Set of devices to allocate
portal.context.defineStructParameter(
    "am_devices", "AM Devices", [],
    multiValue=True,
    min=2,
    multiValueTitle="Attenuator Matrix Devices.",
    members=[
        portal.Parameter(
            "id",
            "Device Name",
            portal.ParameterType.STRING,
            am_devices[0], am_devices,
            longDescription="AM devices to connect via RF matrix."
        ),
    ])


portal.context.defineParameter("start_vnc", 
                               "Start X11 VNC on all compute nodes",
                               portal.ParameterType.BOOLEAN, True)

# Bind parameters
params = portal.context.bindParameters()


# Now verify the parameters.
portal.context.verifyParameters()

# Get request object
request = portal.context.makeRequestRSpec()

# Declare that we may be starting X11 VNC on the compute nodes.
if params.start_vnc:
    request.initVNC()


matrix_nodes = [] 

# Request am nodes
for device in params.am_devices:
    node = request.RawPC(device.id)
    node.component_id = device.id
    node.hardware_type = NODE_TYPE
    node.disk_image = disk_image # r we using same disk image?
    matrix_nodes.append(node)
    node.Desire("rf-controlled", 1)
    if params.start_vnc:
        node.startVNC()
    
    




""" # create rf links
idx = 1
rflink = request.RFLink("rflink{}".format(idx))
for i in range(len(matrix_nodes)):
    node = matrix_nodes[i]
    rflink.addInterface(node.addInterface("{}rf{}".format(node.name,idx))) """

idx = 1
rflink = request.RFLink("rflink{}".format(idx))
node1 = matrix_nodes[0]
node2 = matrix_nodes[1]
rflink.addInterface(node1.addInterface("{}rf{}".format(node1.name,idx)))
rflink.addInterface(node2.addInterface("{}rf{}".format(node2.name,idx)))




portal.context.printRequestRSpec()